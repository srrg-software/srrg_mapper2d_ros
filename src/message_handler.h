#pragma once

#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/LaserScan.h>

#include <tf/transform_listener.h>

#include "srrg_messages/laser_message.h"

using namespace std;
using namespace srrg_core;

typedef message_filters::sync_policies::ApproximateTime<nav_msgs::Odometry, sensor_msgs::LaserScan> MyOdomScanSyncPolicy;

class MessageHandler {
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  MessageHandler(tf::TransformListener* listener_);
  ~MessageHandler();
  //void laser_callback(const sensor_msgs::LaserScan::ConstPtr& msg);  
  void odom_laser_callback(const nav_msgs::Odometry::ConstPtr& odommsg, const sensor_msgs::LaserScan::ConstPtr& scanmsg);  

  // setter & getter
  inline std::list<LaserMessage*>& laser_msgs()  { return _laser_msgs; }
  inline Eigen::Isometry2f laserOffset() { return _laser_offset;}
  inline void setBaseLinkFrame(const std::string base_link_frame_id_){_base_link_frame_id = base_link_frame_id_;}
  inline void setLaserTopic(const std::string laser_topic_){_laser_topic = laser_topic_;}

private:
  tf::TransformListener* _listener;
  std::list<LaserMessage*> _laser_msgs;
  Eigen::Isometry2f _laser_offset;

  std::string _base_link_frame_id;
  std::string _laser_topic;
};
