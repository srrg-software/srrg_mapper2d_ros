#include "occupancy_grid_server.h"

OccupancyGridServer::OccupancyGridServer(GraphOccupancyGrid* graph_occ_grid){
  _graph_occ_grid = graph_occ_grid;
  
  // Default values
  _map_frame = "/map";
  _map_topic = "/map";
}

void OccupancyGridServer::init(){
  _pub_map = _nh.advertise<nav_msgs::OccupancyGrid>(_map_topic, 1, true);
  _pub_map_metadata = _nh.advertise<nav_msgs::MapMetaData>(_map_topic + "_metadata", 1, true);
  _map_server = _nh.advertiseService(_map_topic, &OccupancyGridServer::mapCallback, this);
}

void OccupancyGridServer::publish(){

  nav_msgs::OccupancyGrid map_msg;
  createMapMessage(map_msg);
  
  _pub_map.publish(map_msg);
  _pub_map_metadata.publish(map_msg.info);
}


bool OccupancyGridServer::mapCallback(nav_msgs::GetMap::Request &req, nav_msgs::GetMap::Response &res){

  nav_msgs::OccupancyGrid map_msg;
  createMapMessage(map_msg);

  res.map = map_msg;
}

void OccupancyGridServer::createMapMessage(nav_msgs::OccupancyGrid &map_msg){
  if (!_graph_occ_grid){
    std::cerr << "Error to publish occupancy grid. GraphOccupancyGrid not properly set." << std::endl;
    return;
  }
      
  nav_msgs::MapMetaData map_metadata_msg;

  map_metadata_msg.map_load_time = ros::Time::now();
  map_metadata_msg.resolution = _graph_occ_grid->resolution();
  map_metadata_msg.width = _graph_occ_grid->size().x();
  map_metadata_msg.height = _graph_occ_grid->size().y();
  map_metadata_msg.origin.position.x = _graph_occ_grid->origin().x();
  map_metadata_msg.origin.position.y = _graph_occ_grid->origin().y();
  map_metadata_msg.origin.position.z = _graph_occ_grid->origin().z();
  map_metadata_msg.origin.orientation.x = 0;
  map_metadata_msg.origin.orientation.y = 0;
  map_metadata_msg.origin.orientation.z = 0;
  map_metadata_msg.origin.orientation.w = 1;
	    
  map_msg.header.stamp = ros::Time::now();
  map_msg.header.frame_id = _map_frame;
  map_msg.info = map_metadata_msg;
  map_msg.data.resize(_graph_occ_grid->size().x() * _graph_occ_grid->size().y());
    
  for(int r = 0; r < _graph_occ_grid->frequencyMapImage().rows; r++) {
    for(int c = 0; c < _graph_occ_grid->frequencyMapImage().cols; c++) {
      map_msg.data[ _graph_occ_grid->frequencyMapImage().rows * c + r] = _graph_occ_grid->frequencyMapImage().at<unsigned char>(r,c);
    }
  }

}
