#include "graph_occupancy_grid.h"

GraphOccupancyGrid::GraphOccupancyGrid(OptimizableGraph *graph){
  _graph = graph;

  _resolution = 0.05;
  _occ_threshold = 0.65;
  _free_threshold = 0.196;
  _max_range = -1.0;
  _usable_range = -1.0;
  _infinity_filling_range = -1.0;
  _gain = 1;
  _square_size = 1;
  _angle = 0;
  _rows = 0, _cols = 0;
  
  _free_cell = 0;
  _unknown_cell = -1;
  _occupied_cell = 100;

}


void GraphOccupancyGrid::compute(){
  if (!_graph) {
    std::cerr << "Error to compute occupancy grid. You must set a graph." << std::endl;
    return;
  }
    
  // Sort vertices
  std::vector<int> vertexIds(_graph->vertices().size());
  int k = 0;
  for(OptimizableGraph::VertexIDMap::iterator it = _graph->vertices().begin(); it != _graph->vertices().end(); ++it) {
    vertexIds[k++] = (it->first);
  }  
  sort(vertexIds.begin(), vertexIds.end());
  
  /************************************************************************
   *                          Compute map size                            *
   ************************************************************************/
  // Check the entire graph to find map bounding box
  Eigen::Matrix2d boundingBox = Eigen::Matrix2d::Zero();
  std::vector<RobotLaser*> robotLasers;
  std::vector<SE2> robotPoses;
  double xmin=std::numeric_limits<double>::max();
  double xmax=std::numeric_limits<double>::min();
  double ymin=std::numeric_limits<double>::max();
  double ymax=std::numeric_limits<double>::min();

  SE2 baseTransform(0,0,_angle);
  bool initial_pose_found = false;
  SE2 initial_pose;
  for(size_t i = 0; i < vertexIds.size(); ++i) {
    OptimizableGraph::Vertex *_v = _graph->vertex(vertexIds[i]);
    VertexSE2 *v = dynamic_cast<VertexSE2*>(_v);
    if(!v) { continue; }
    if (v->fixed() && !initial_pose_found){
      initial_pose_found = true;
      initial_pose = baseTransform*v->estimate();
    }
    
    OptimizableGraph::Data *d = v->userData();
      
    while(d) {
      RobotLaser *robotLaser = dynamic_cast<RobotLaser*>(d);
      if(!robotLaser) {
	d = d->next();
	continue;
      }      
      robotLasers.push_back(robotLaser);
      SE2 transformed_estimate = baseTransform*v->estimate();
      robotPoses.push_back(transformed_estimate);
      double x = transformed_estimate.translation().x();
      double y = transformed_estimate.translation().y();
      
      xmax = xmax > x + _usable_range ? xmax : x + _usable_range;
      ymax = ymax > y + _usable_range ? ymax : y + _usable_range;
      xmin = xmin < x - _usable_range ? xmin : x - _usable_range;
      ymin = ymin < y - _usable_range ? ymin : y - _usable_range;
	
      d = d->next();
    }
  }
    
  boundingBox(0,0)=xmin;
  boundingBox(0,1)=xmax;
  boundingBox(1,0)=ymin;
  boundingBox(1,1)=ymax;
    
  if(robotLasers.size() == 0)  {
    std::cout << "No laser scans found ... quitting!" << std::endl;
    return;
  }

  /************************************************************************
   *                          Compute the map                             *
   ************************************************************************/
  // Create the map
  float required_rows = (boundingBox(0, 1) - boundingBox(0, 0))/ _resolution;
  float required_cols = (boundingBox(1, 1) - boundingBox(1, 0))/ _resolution;
  if(_rows != 0 && _cols != 0) {
    setSize(Eigen::Vector2i(_rows, _cols));
    if (_rows < required_rows || _cols < required_cols){
      std::cout << "Warning: map not fitting on desired rows and cols" << std::endl;
    }
  } else {
    setSize(Eigen::Vector2i(required_rows, required_cols));
  } 

  if(_size.x() == 0 || _size.y() == 0) {
    std::cout << "Zero map size ... quitting!" << std::endl;
    return;
  }
    
  Eigen::Vector2f offset(boundingBox(0, 0),boundingBox(1, 0));
  FrequencyMapCell unknownCell;
  _fmap = FrequencyMap(_resolution, offset, _size, unknownCell);

  for (size_t i = 0; i < robotPoses.size(); ++i)
    _fmap.integrateScan(robotLasers[i], robotPoses[i],
			_max_range, _usable_range, _infinity_filling_range,
			_gain, _square_size);
      
      
  /************************************************************************
   *                          Generate map image                          *
   ************************************************************************/
  _frequency_map_image = cv::Mat(_fmap.rows(), _fmap.cols(), CV_8UC1);
  _frequency_map_image.setTo(cv::Scalar(0));
  for(int c = 0; c < _fmap.cols(); c++) {
    for(int r = 0; r < _fmap.rows(); r++) {
      // draw bounding box
      if (c==0 || r==0 || c==_fmap.cols()-1 || r==_fmap.rows()-1) {
	_frequency_map_image.at<unsigned char>(r, c) = _occupied_cell;
	continue;
      }
      if(_fmap(r, c).misses() == 0 && _fmap(r, c).hits() == 0) {
	_frequency_map_image.at<unsigned char>(r, c) = _unknown_cell;
      } else {
	float fraction = (float)_fmap(r, c).hits()/(float)(_fmap(r, c).hits()+_fmap(r, c).misses());
	if (fraction < _free_threshold)
	  _frequency_map_image.at<unsigned char>(r, c) = _free_cell;
	else if (_occ_threshold && fraction > _occ_threshold)
	  _frequency_map_image.at<unsigned char>(r, c) = _occupied_cell;
	else {
	  //float val = 255.0f*(1.0f - fraction);
	  //_frequency_map_image.at<unsigned char>(r, c) = (unsigned char)val;
	  _frequency_map_image.at<unsigned char>(r, c) = _unknown_cell;
	}
      }
    }
  }

  // Setting map origin
  _origin = Eigen::Vector3f(0.0f, 0.0f, 0.0f);
  if (initial_pose_found){
    Eigen::Vector2i originMap = _fmap.world2map(Eigen::Vector2f(0,0));

    _origin = Eigen::Vector3f( - _resolution * originMap.x(),
			       - _resolution * originMap.y(),
			       0.0f);
  }
  
}


void GraphOccupancyGrid::saveMap(std::string map_filename){
    
    // Build png image from frequency map
    // Unknown value based on free_threshold
    unsigned char free = 255;
    unsigned char occupied = 0;
    unsigned char unknown = (1.0 - _free_threshold) * 255.f;

    // map_image to be saved as png is rotated 90deg anti-clockwise wrt frequency_map_image
    cv::Mat map_image(_frequency_map_image.cols, _frequency_map_image.rows, CV_8UC1);
    map_image.setTo(cv::Scalar(0));

    for(int r = 0; r < _frequency_map_image.rows; r++) {
      for(int c = 0; c < _frequency_map_image.cols; c++) {
	if(_frequency_map_image.at<unsigned char>(r, c) == _unknown_cell) 
	  map_image.at<unsigned char>(map_image.rows-1-c, r) = unknown;  
	else if (_frequency_map_image.at<unsigned char>(r, c) == _free_cell) 
	  map_image.at<unsigned char>(map_image.rows-1-c, r) = free;   
	else if (_frequency_map_image.at<unsigned char>(r, c) == _occupied_cell) 
	  map_image.at<unsigned char>(map_image.rows-1-c, r) = occupied;
      }
    }
    
    cv::imwrite(map_filename + ".png", map_image);
    
    // Write yaml file
    std::ofstream ofs(std::string(map_filename + ".yaml").c_str());
  
    ofs << "image: " << map_filename << ".png" << std::endl
	<< "resolution: " << _resolution << std::endl
	<< "origin: [" << _origin.x() << ", " << _origin.y() << ", " << _origin.z() << "]" << std::endl
	<< "negate: 0" << std::endl
	<< "occupied_thresh: " << _occ_threshold << std::endl
	<< "free_thresh: " << _free_threshold << std::endl;

    double sizeX = map_image.rows * _resolution;
    double sizeY = map_image.cols * _resolution;

    std::ofstream stage_stream(std::string(map_filename + ".world").c_str());
    stage_stream << "floorplan (" << std::endl;
    stage_stream << " name \"" << map_filename << "\"" << std::endl;
    stage_stream << " bitmap \"" << map_filename << ".png\"" << std::endl;
    stage_stream << " size [" << sizeY << " " << sizeX << " 0.5]" << std::endl;
    stage_stream << " pose [" << sizeY/2+_origin.x() << " " << sizeX/2+_origin.y() << " 0 0 ]" << std::endl;
    stage_stream << ")" << std::endl;
    stage_stream.close();

  }
