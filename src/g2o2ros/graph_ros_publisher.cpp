#include "graph_ros_publisher.h"


GraphROSPublisher::GraphROSPublisher(OptimizableGraph *graph){
  _graph = graph;

  _map_frame = "/map";
  _odom_frame = "/odom";
  
  _rate = 10;
  _publish_tf = false;
  _publish_graph = false;
  
  _estimate = Eigen::Isometry2f::Identity();
  _odom = Eigen::Isometry2f::Identity();
}

void GraphROSPublisher::start(bool publish_graph, bool publish_tf){
  _publish_graph = publish_graph;
  _publish_tf = publish_tf;
  
  if (_publish_graph){
    _pub_trajectory = _nh.advertise<geometry_msgs::PoseArray>("trajectory", 1);
    _pub_lasermap = _nh.advertise<sensor_msgs::PointCloud>("lasermap", 1);
  }

  if (_publish_tf)
    _tf_publish_thread = std::thread(&GraphROSPublisher::publishTF, this);

}

void GraphROSPublisher::stop(){
  if (_publish_tf)
    _tf_publish_thread.join();
}

void GraphROSPublisher::publishTF(){

  ros::Rate loop_rate(1e3/_rate);
    
  while (ros::ok()){
    Eigen::Isometry2f delta_map_odom = _estimate*_odom.inverse();
    Eigen::Vector3f delta_map_odom_v = t2v(delta_map_odom);
    tf::Transform tmp_tf(tf::createQuaternionFromYaw(delta_map_odom_v.z()),
			 tf::Vector3(delta_map_odom_v.x(),
				     delta_map_odom_v.y(),
				     0.0));
    
    tf::StampedTransform map_to_odom(tmp_tf,
				     ros::Time::now(),
				     _map_frame, _odom_frame);
      
    _tf_broadcaster.sendTransform(map_to_odom);
      
    loop_rate.sleep();
  }
  
  std::cerr << "Publish Transform thread finished." << std::endl;
    
}

void GraphROSPublisher::publishGraph(){

  assert(_graph && "Cannot publish: undefined graph");
  if (!_publish_graph)
    return;

  geometry_msgs::PoseArray traj;
  sensor_msgs::PointCloud pcloud;
  traj.poses.resize(_graph->vertices().size());
  pcloud.points.clear();
  int i = 0;
  for (OptimizableGraph::VertexIDMap::iterator it=_graph->vertices().begin(); it!=_graph->vertices().end(); ++it) {
    VertexSE2* v = (VertexSE2*) (it->second);
    traj.poses[i].position.x = v->estimate().translation().x();
    traj.poses[i].position.y = v->estimate().translation().y();
    traj.poses[i].position.z = 0;
    traj.poses[i].orientation = tf::createQuaternionMsgFromYaw(v->estimate().rotation().angle());

    RobotLaser *laser = dynamic_cast<RobotLaser*>(v->userData());
    if (laser){
      RawLaser::Point2DVector vscan = laser->cartesian();
      Eigen::Isometry2f laser_pose = laser->laserParams().laserPose.toIsometry().cast<float>();
      Eigen::Isometry2f scan_pose = v->estimate().toIsometry().cast<float>() * laser_pose;

      //we skip some points
      for (size_t s=0; s < vscan.size(); s=s+10){
	Eigen::Isometry2f laser_point = Eigen::Isometry2f::Identity();
	laser_point.translation().x() = vscan[s].x();
	laser_point.translation().y() = vscan[s].y();

	Eigen::Isometry2f transformed_point = scan_pose * laser_point;
	geometry_msgs::Point32 point;
	point.x = transformed_point.translation().x();
	point.y = transformed_point.translation().y();
	pcloud.points.push_back(point);
      }
    }
    i++;
  }
  
  traj.header.frame_id = _map_frame;
  traj.header.stamp = ros::Time::now();
  pcloud.header.frame_id = traj.header.frame_id;
  pcloud.header.stamp = ros::Time::now();
  _pub_trajectory.publish(traj);
  _pub_lasermap.publish(pcloud);
  
}
