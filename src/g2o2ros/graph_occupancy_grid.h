#pragma once

//g2o includes
#include "g2o/core/optimizable_graph.h"
#include "g2o/types/data/robot_laser.h"
#include "g2o/types/slam2d/vertex_se2.h"

#include <opencv2/opencv.hpp>

#include "frequency_map.h"

#include <string>
#include <iostream> 

using namespace g2o;

class GraphOccupancyGrid {

public:

  GraphOccupancyGrid(OptimizableGraph *graph);

  inline void setResolution(float resolution) {_resolution = resolution;}
  inline float resolution() const {return _resolution;}
  inline void setOccThreshold(float occ_threshold) {_occ_threshold = occ_threshold;}
  inline float occThreshold() const {return _occ_threshold;}
  inline void setFreeThreshold(float free_threshold) {_free_threshold = free_threshold;}
  inline float freeThreshold() const {return _free_threshold;}
  inline void setMaxRange(float max_range) {_max_range = max_range;}
  inline float maxRange() const {return _max_range;}
  inline void setUsableRange(float usable_range) {_usable_range = usable_range;}
  inline float usableRange() const {return _usable_range;}
  inline void setInfinityFillingRange(float infinity_filling_range) {_infinity_filling_range = infinity_filling_range;}
  inline float infinityFillingRange() const {return _infinity_filling_range;}
  inline void setGain(float gain) {_gain = gain;}
  inline float gain() const {return _gain;}
  inline void setSquareSize(float square_size) {_square_size = square_size;}
  inline float squareSize() const {return _square_size;}
  inline void setAngle(float angle) {_angle = angle;}
  inline float angle() const {return _angle;}
  inline void setRows(float rows) {_rows = rows;}
  inline float rows() const {return _rows;}
  inline void setCols(float cols) {_cols = cols;}
  inline float cols() const {return _cols;}

  inline void setOrigin(const Eigen::Vector3f& origin) {_origin = origin;}
  inline Eigen::Vector3f& origin() {return _origin;}

  inline void setSize(const Eigen::Vector2i& size) {_size = size;}
  inline Eigen::Vector2i& size() {return _size;}

  inline cv::Mat& frequencyMapImage() {return _frequency_map_image;}
  
  void compute();

  void saveMap(std::string map_filename);
  
protected:

  OptimizableGraph *_graph;
  FrequencyMap _fmap;
  cv::Mat _frequency_map_image; //Stores map to be published (compatible with map_server)
    
  // Occupancy Grid parameters
  float _resolution;
  float _occ_threshold;
  float _free_threshold;
  float _max_range;
  float _usable_range;
  float _infinity_filling_range;
  float _gain;
  float _square_size;
  float _angle;
  float _rows, _cols;

  Eigen::Vector2i _size;

  // Management of map origin
  Eigen::Vector3f _origin;

  // Cells values compliant with ROS map_server
  unsigned char _free_cell;
  unsigned char _unknown_cell;
  unsigned char _occupied_cell;

};
