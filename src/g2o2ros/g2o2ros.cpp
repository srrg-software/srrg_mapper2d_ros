#include <iostream> 

#include "g2o/core/sparse_optimizer.h"

#include "g2o/stuff/command_args.h"

#include "graph_occupancy_grid.h"

using namespace std;
using namespace g2o;

int main(int argc, char **argv) {
  /************************************************************************
   *                          Input handling                              *
   ************************************************************************/
  float rows, cols, gain, square_size;
  float resolution, max_range, usable_range, infinity_filling_range, angle, occ_threshold, free_threshold;
  string g2oFilename, mapFilename;
  g2o::CommandArgs arg;
  arg.param("resolution", resolution, 0.05f, "resolution of the map (how much is in meters a pixel)");
  arg.param("occ_threshold", occ_threshold, .65f, "threshold to apply to the frequency map (values under the threshold are discarded)");
  arg.param("rows", rows, 0, "impose the resulting map to have this number of rows");
  arg.param("cols", cols, 0, "impose the resulting map to have this number of columns");
  arg.param("max_range", max_range, -1.0f, "max laser range to consider for map building");
  arg.param("usable_range", usable_range, -1.0f, "usable laser range for map building");
  arg.param("infinity_filling_range", infinity_filling_range, -1.0f, "beams going to infinity will be filled as empty up to this range");
  arg.param("gain", gain, 1, "gain to impose to the pixels of the map");
  arg.param("square_size", square_size, 1, "square size of the region where increment the hits");
  arg.param("angle", angle, 0, "rotate the map of x degrees");
  arg.param("free_threshold", free_threshold, 0.196f, "Pixels with occupancy probability less than this threshold are considered completely free");

  arg.paramLeftOver("input_graph.g2o", g2oFilename, "", "input g2o graph to use to build the map", false);
  arg.paramLeftOver("output_map", mapFilename, "", "output filename where to save the map (without extension)", false);  
  arg.parseArgs(argc, argv);

  angle = angle*M_PI/180.0;

  /************************************************************************
   *                          Loading Graph                               *
   ************************************************************************/
  // Load graph
  SparseOptimizer *graph = new SparseOptimizer();
  graph->load(g2oFilename.c_str());

  GraphOccupancyGrid graph2occ(graph);
  graph2occ.setResolution(resolution);
  graph2occ.setOccThreshold(occ_threshold);
  graph2occ.setFreeThreshold(free_threshold);
  graph2occ.setMaxRange(max_range);
  graph2occ.setUsableRange(usable_range);
  graph2occ.setInfinityFillingRange(infinity_filling_range);
  graph2occ.setGain(gain);
  graph2occ.setSquareSize(square_size);
  graph2occ.setAngle(angle);
  graph2occ.setRows(rows);
  graph2occ.setCols(cols);

  graph2occ.compute();
  graph2occ.saveMap(mapFilename);
  
}
